FROM openjdk:8-alpine
COPY build/libs/hello-world-0.1.0.jar Hello-World.jar
ENTRYPOINT ["java","-jar","/Hello-World.jar"]

